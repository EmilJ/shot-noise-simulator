import random 
import numpy as np
import pydub
import math

shots_per_s = 2
seconds = 3
shot_num = math.floor(shots_per_s*seconds)
firerate = 267/60 #*10 # The fun constant! 
shot_times_ms = []

# Check if we can fit in the deagle sound comfortably
# Remove and increase shots per second for a shitstorm
assert(seconds*firerate > 1.3*shot_num)

shot1 = pydub.AudioSegment.from_file("deagle_01.wav")
shot2 = pydub.AudioSegment.from_file("deagle_02.wav")

track = pydub.AudioSegment.silent(1000*seconds+max(len(shot1), len(shot2)))

for i in range(shot_num):
    shot_time = random.random()*seconds*1000
    check = True
    # Reroll until we get a sufficiently spaced out sound
    while check:
        check = False
        for other_time in shot_times_ms:
            if abs(other_time - shot_time) < 1000/firerate:
                check = True
                shot_time = random.random()*seconds*1000
                # print("reroll")
                break

    shot = pydub.AudioSegment.silent(duration=shot_time)
    shot_times_ms.append(shot_time)
    if random.randint(0, 1):
        shot += shot1
    else:
        shot += shot2
    track = track.overlay(shot)

track.export("shot.mp3", format="mp3")

diffs = []
shot_times_ms = sorted(shot_times_ms)
for i in range(1,len(shot_times_ms)):
    diffs.append(shot_times_ms[i]-shot_times_ms[i-1])

print(diffs)
print(1000/firerate)
assert all([a > 1000/firerate for a in diffs])